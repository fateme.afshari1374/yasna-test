package com.neillon.dogs.view.fragments


import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.opengl.Visibility
import android.os.Bundle
import android.telephony.SmsManager
import android.view.*
import android.view.View.VISIBLE
import android.widget.RadioGroup
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil


import com.neillon.dogs.R
import com.neillon.dogs.databinding.FragmentDetailBinding

import kotlinx.android.synthetic.main.fragment_detail.*


class DetailFragment : Fragment() {

    private lateinit var databinding: FragmentDetailBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        databinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_detail,
            container,
            false
        )
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        r1.setOnClickListener { l ->
            view.visibility = View.VISIBLE
            t2.visibility = View.VISIBLE
            t3.visibility = View.VISIBLE
            radioGroup.visibility = View.VISIBLE
        }
        radioGroup.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                t4.visibility = View.VISIBLE
                button.visibility = View.VISIBLE
                if (r2.isChecked) {
                    t4.text =
                        "no need for pre registration.people can stop by your bussiness at any ..."
                } else if (r3.isChecked) {
                    t4.text =
                        "set a work schedule and people will need  to book any of the avilable time slots ..."

                }
            })
    }


}
