package com.neillon.dogs.view.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation

import com.neillon.dogs.R
import com.neillon.dogs.model.CategoryModel
import com.neillon.dogs.view.adapters.ExpandableListAdapter
import com.neillon.dogs.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment() {

    private lateinit var viewModel: ListViewModel
    private lateinit var adapter :ExpandableListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
      //  setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        viewModel.refresh()

        observeViewModel()
    }



    private fun observeViewModel() {
        viewModel.categories.observe(this, Observer { categories: List<CategoryModel> ->
            categories?.let {
                categoryList.visibility = View.VISIBLE
                adapter = ExpandableListAdapter(this.requireContext(), categories)
                categoryList!!.setAdapter(adapter)
            }
        })

        viewModel.loadError.observe(this, Observer {
            it?.let {
                listError.visibility = if (it) View.VISIBLE else View.GONE
            }
        })

        viewModel.loading.observe(this, Observer {
            it?.let {
                progressLoading.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                    categoryList.visibility = View.GONE
                }
            }
        })
        categoryList.setOnChildClickListener { expandableListView, view, i, i2, l ->
            val action = ListFragmentDirections.actionDetailFragment()
            Navigation.findNavController(view).navigate(action)
            false }
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return super.onOptionsItemSelected(item)
    }
}
