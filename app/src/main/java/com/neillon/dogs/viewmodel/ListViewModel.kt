package com.neillon.dogs.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.neillon.dogs.model.ApiService
import com.neillon.dogs.model.CategoryModel
import com.neillon.dogs.model.Model
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ListViewModel(application: Application) : BaseViewModel(application) {

    private val ApiService = ApiService()
    private val disposable = CompositeDisposable()

    val categories = MutableLiveData<List<CategoryModel>>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()


    fun refresh() {
        fetchFromRemote()
    }

    private fun fetchFromRemote() {
        loading.value = true
        disposable.add(
            ApiService.getCategory()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Model>() {
                    override fun onSuccess(response: Model) {
                        setCategoriesRetrived(response.result)
                    }

                    override fun onError(error: Throwable) {
                        loading.value = false
                        loadError.value = true
                        error.printStackTrace()
                        Log.e("CATEGORY_API", error.message)
                    }
                })
        )
    }

    private fun setCategoriesRetrived(categoryList: List<CategoryModel>) {
        categories.value = categoryList
        loadError.value = false
        loading.value = false
    }


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}