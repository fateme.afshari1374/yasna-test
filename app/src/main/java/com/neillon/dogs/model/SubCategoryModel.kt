package com.neillon.dogs.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class SubCategoryModel(
    @ColumnInfo(name = "id")
    @SerializedName("id")
    val id: String?,

    @ColumnInfo(name = "title")
    @SerializedName("title")
    val title: String?

    )