package com.neillon.dogs.model

import android.view.Display
import io.reactivex.Single
import retrofit2.http.GET

interface Api {


    @GET("Category")
    fun getCategories(): Single<Model>
}