package com.neillon.dogs.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class Model(
    @ColumnInfo(name = "result")
    @SerializedName("result")
    val result: List<CategoryModel>
)